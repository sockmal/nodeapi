require('dotenv').config(); //Import dotenv Library to use environment variables
const express = require('express'); // Import express Framework
const app = express(); //Init express module

var enableCORS = function(req, res, next) {
  res.set("Access-Control-Allow-Origin", "*");
  res.set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");

  // This will be needed.
  res.set("Access-Control-Allow-Headers", "Content-Type");

  next();
 }

app.use(express.json()); // Añade un preprocesador para que pueda recoger el body como json ya que sino llega como undefined.
app.use(enableCORS);

const userController = require('./controllers/UserController');
// Define controller to Authentication
const authController = require('./controllers/AuthController');

// Define controller to Account Controller
const accountControler = require('./controllers/AccountController');

const port = process.env.PORT || 3000; // Le asignamos una variable por defecto y sino usa una por defecto.
app.listen(port); // pone en marcha el servidor

console.log("API escuchando en el puerto " + port);

app.get("/apitechu/v1/hello",
  function(req, res) {
    console.log("GET /apitechu/v1/hello");
    res.send({"msg":"hola estamos en la techu"});
  }
)

// Creamos nueva ruta con parámetros y recogeremos
// Parámetros, Query String, Header y Body
// Desde Postman lanzaremos localhost:3000/apitechu/v1/monstruo/1/2?header1=value1&header2=value2
// Crear el body con application/json en raw, añadir header y parámetros
app.post("/apitechu/v1/monstruo/:p1/:p2",
  function(req, res) {
    console.log("Parametros");
    console.log(req.params);

    console.log("Query Sring");
    console.log(req.query);

    console.log("Headers");
    console.log(req.headers);

    console.log("Body");
    console.log(req.body);
  }
)

// Fuction to get users
// usuarios.json has been created with https://www.mockaroo.com/
app.get("/apitechu/v1/users", userController.getUsersV1);

// Get users V2
app.get("/apitechu/v2/users", userController.getUsersV2);

// Get users by ID V2
app.get("/apitechu/v2/users/:id", userController.getUsersByIdV2);

// Function to create users
app.post("/apitechu/v1/users", userController.createUsersV1);

// Function to create users V2
app.post("/apitechu/v2/users", userController.createUserV2);

// Function to delete users
app.delete("/apitechu/v1/users/:id", userController.deleteUserV1);

// Function to Login users
app.post("/apitechu/v1/login", authController.loginUsersV1);

// Login user V2 with real database
app.post("/apitechu/v2/login", authController.loginUserV2);

// Fuction to Logout users
app.post("/apitechu/v1/login/:id", authController.logoutUsersV1);

// Fuction to Logout users
app.post("/apitechu/v2/logout/:id", authController.logoutUserV2);

// Get accounts by ID V2
app.get("/apitechu/v2/account/:id", accountControler.getAccountByIdV2);