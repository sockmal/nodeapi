const fs = require('fs');

function writeUserDataToFile(data) {
  console.log("writeUserDataToFile");

  var jsonUserData = JSON.stringify(data);

  fs.writeFile("./usuarios.json", jsonUserData, "utf8",
    function(error) {
      if (error) {
        console.log(err);
      } else {
        console.log("Users Database Updated");
      }
    }
  )
}

module.exports.writeUserDataToFile = writeUserDataToFile;