// CONSTANTS
const io =require("../utils/io"); // Declaramos el nuevo modulo para recoger las funciones
const requestJson = require('request-json'); // Para cargar la librería de request-json
const crypt =require("../utils/crypt"); // Cargamos librería de encriptación

const baseMLABUrl = "https://api.mlab.com/api/1/databases/apitechumal12ed/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;

// LOGIN FUNCTIONS

function loginUsersV1(req, res) {
  console.log("POST /apitechu/v1/login");

  console.log(req.body);
  console.log(req.body.email);
  console.log(req.body.password);

  var loginUser = {
    "email": req.body.email,
    "password" : req.body.password
  }

  // Load Database of users
  var users = require('../usuarios.json');
  var userID = ""
  flagUserLogin = false

  // Search email in users
  console.log("Usando Array findIndex");
  var indexOfElement = users.findIndex(
    function(user){
      console.log("comparando " + user.email + " y " +   req.body.email);
      console.log("comparando " + user.password + " y " +   req.body.password);

      // Insert user logged only when email and password have been found
      if (user.email == req.body.email && user.password == user.password) {
        user.logged = true
        // Get current userID
        userID = user.id;
      }
      return user.email == req.body.email && user.password == user.password
    }
  )

  // console.log("indexOfElement es " + indexOfElement);
  if (indexOfElement >= 0) {
    i = indexOfElement;
    // set flagUserLogin to true
    flagUserLogin = true;
  }

  if (flagUserLogin == true) {
    console.log("The login is successful, insert logged true");
      // Insert logged user with true
      io.writeUserDataToFile(users);
      res.send({"msg":"Login Successful", "userID": userID});
  } else {
    // Not found
    console.log("Incorrect Login");
    res.send({"msg":"Incorrect Login"});
  }
}

function loginUserV2(req, res) {
  console.log("POST /apitechu/v2/login");

  var query = "q=" + JSON.stringify({"email": req.body.email});
  console.log("query es " + query);

  httpClient = requestJson.createClient(baseMLABUrl);
  httpClient.get("user?" + query + "&" + mLabAPIKey,
    function(err, resMLab, body) {

      var isPasswordcorrect =
        crypt.checkPassword(req.body.password, body[0].password);
      console.log("Password correct is " + isPasswordcorrect);

      if (!isPasswordcorrect) {
        var response = {
          "mensaje" : "Incorrect Login, email or passsword try again..."
        }
        res.status(401);
        res.send(response);
      } else {
        console.log("Got a user with that email and password, logging in");
        var id = Number.parseInt(body[0].id);
        query = "q=" + JSON.stringify({"id": id});
        console.log("Query for put is " + query);
        var putBody = '{"$set":{"logged":true}}';
        httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
          function(errPUT, resMLabPUT, bodyPUT) {
            console.log("PUT done");
            var response = {
              "msg" : "User Login Success",
              "idUsuario" : body[0].id
            }
            res.send(response);
          }
        )
      }
    }
  );
 }

function logoutUsersV1(req, res) {
  console.log("POST /apitechu/v1/logout/:id");

  console.log(req.params);
  console.log(req.params.id);

  // Load Database of users
  var users = require('../usuarios.json');

  // Search email in users
  console.log("Usando Array findIndex");
  var indexOfElement = users.findIndex(
    function(user){
      console.log("comparando " + user.logged + " con " + user.id + " y " +   req.params.id);

      return user.id == req.params.id && user.logged == true
    }
  )

  console.log("indexOfElement es " + indexOfElement);
  if (indexOfElement >= 0) {
    delete users[indexOfElement].logged;

    console.log("The logout is successful, insert logged true");
    io.writeUserDataToFile(users);
    res.send({"msg":"logout Successful with ", "User ID " : req.params.id});
  } else {
    // Not found
    console.log("Incorrect logout");
    res.send({"msg":"Incorrect logout"});
  }
}

function logoutUserV2(req, res) {
  console.log("POST /apitechu/v2/logout/:id");
 
  var id = Number.parseInt(req.params.id);
  var query = "q=" + JSON.stringify({"id": id});
  console.log("query es " + query);
 
  httpClient = requestJson.createClient(baseMLABUrl);
  httpClient.get("user?" + query + "&" + mLabAPIKey,
    function(err, resMLab, body) {
      if (body.length == 0) {
        var response = {
          "mensaje" : "Logout incorrecto, usuario no encontrado"
        }
        res.send(response);
      } else {
        console.log("Got a user with that id, logging out");
        id = Number.parseInt(body[0].id);
        query = "q=" + JSON.stringify({"id": id});
        console.log("Query for put is " + query);
        var putBody = '{"$unset":{"logged":""}}'
        httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
          function(errPUT, resMLabPUT, bodyPUT) {
            console.log("PUT done");
            var response = {
              "msg" : "Usuario deslogado",
              "idUsuario" : body[0].id
            }
            res.send(response);
          }
        )
      }
    }
  );
 }

module.exports.loginUsersV1 = loginUsersV1;
module.exports.loginUserV2 = loginUserV2;
module.exports.logoutUsersV1 = logoutUsersV1;
module.exports.logoutUserV2 = logoutUserV2;