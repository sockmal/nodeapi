// CONSTANTS
const io =require("../utils/io"); // Declaramos el nuevo modulo para recoger las funciones
const requestJson = require('request-json'); // Para cargar la librería de request-json
const crypt =require("../utils/crypt"); // Cargamos librería de encriptación

const baseMLABUrl = "https://api.mlab.com/api/1/databases/apitechumal12ed/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;

// FUNCTIONS
function getUsersV1(req, res) {
  console.log("GET /apitechu/v1/users");

  var result = {};
  var users = require('../usuarios.json');

  if (req.query.$count == "true") {
    console.log("Count needed");
    result.count = users.length;
  }

  result.users = req.query.$top ?
    users.slice(0, req.query.$top) : users;

  res.send(result);
}

function getUsersV2(req, res) {
  console.log("GET /apitechu/v2/users");

  var httpClient = requestJson.createClient(baseMLABUrl);
  console.log("Client created");

  httpClient.get("user?" + mLabAPIKey,
    function(err, resMlab, body) {
      var response = !err ? body : {
        "msg" : "Error obteniendo usuarios"
      }
      console.log(response);
      res.send(response);
    }
  );
}

// Get Users By ID
function getUsersByIdV2(req, res) {
  console.log("GET /apitechu/v2/users/:id")

  var id = Number.parseInt(req.params.id);
  console.log("The user id to get is " + id);
  var query = "q=" + JSON.stringify({"id": id});
  console.log("query is " + query);

  var httpClient = requestJson.createClient(baseMLABUrl);
  console.log("Client created");
  console.log(query);
  // Control the response status
  httpClient.get("user?" + query + "&" + mLabAPIKey,
    function(err, resMlab, body) {
      console.log(body);
      if (err) {
        var response = {
          "msg" : "Error getting user"
        }
        res.status(500);
      } else {
        if (body.length > 0) {
          var response = body[0];
        } else {
          var response = {
            "msg" : "user not found"
          }
          res.status(404);
        }
      }
      res.send(response);
    }
  )
}

function createUsersV1(req, res) {
  console.log("POST /apitechu/v1/users");

  console.log(req.body);
  console.log(req.body.first_name);
  console.log(req.body.last_name);
  console.log(req.body.email);

  var newUser = {
    "first_name": req.body.first_name,
    "last_name": req.body.last_name,
    "email": req.body.email
  }

  console.log(newUser);

  var users = require('../usuarios.json');
  users.push(newUser);
  console.log("Users have been added into array");

  io.writeUserDataToFile(users);

  res.send({"msg":"User has been created"});
}

function createUserV2(req,res) {
  console.log("---------------\nPOST /apitechu/v2/users");

  // console.log(req.body)
  var newUser={
    "id" :req.body.id,
    "first_name" :req.body.first_name,
    "last_name" :req.body.last_name,
    "email" :req.body.email,
    "password" :crypt.hash(req.body.password)
  }
  console.log(newUser);
  var httpClient=requestJson.createClient(baseMLABUrl);
  console.log("Client created");

  httpClient.post("user?"+mLabAPIKey,newUser,
  function(err,resMlab, body){
    if(err){
      var response = {
        "msg": "Error alta usuario"
      }
      res.status(500);
    }else {
      var response = {
        "msg": "Usuario Creado"
      }
      res.status(201);
    }
    res.send(response);
  }
  );
}


function deleteUserV1(req, res) {
  console.log("DELETE /apitechu/v1/users/:id");

  console.log("The user id to delete is " + req.params.id);

  // Charge List of users and user to delete
  var users = require('../usuarios.json');
  var flagUserDelete = "false";

  // 1. Exercise loop with basic For
  // for (var i = 0; i < users.length; i++)  {
  //   if (users[i].id == req.params.id ) {
  //     console.log("Found users into array with ID " + users[i].id);
  //     console.log("User Name is " + users[i].first_name + "and id is " + users[i].id);
  //     flagUserDelete = "true";
  //     break;
  //   }
  // }

  // 2. Exercise loop with for..in
  // for (const prop in users) {
  //   if (users[prop].id == req.params.id ) {
  //     console.log("Found users into array with ID " + users[prop].id);
  //     console.log("User Name is " + users[prop].first_name + "and id is " + users[prop].id);
  //     i = prop;
  //     flagUserDelete = "true";
  //   }
  // }

  // 3. Exercise loop with for...each
  // Don't allow break
  // users.forEach((user, index) => {
  //   if (users[index].id == req.params.id ) {
  //     console.log("Found users into array with ID " + users[index].id);
  //     console.log("User Name is " + users[index].first_name + "and id is " + users[index].id);
  //     i = index;
  //     flagUserDelete = "true";
  //   }
  // });

  // 4. Exercise loop with for...of
  // var iterator = users.entries();
  // var i = 0;

  // for (let user of iterator) {
  //   console.log(user);
  //   if (user[1].id == req.params.id ) {
  //     console.log("Found users into array with ID " + user[1].id);
  //     console.log("User Name is " + user[1].first_name + "and id is " + user[1].id);
  //     flagUserDelete = "true";
  //     i = user[0];
  //     break;
  //   }
  // }

   // 5. Exercise loop with FindIndex
   console.log("Usando Array findIndex");
   var indexOfElement = users.findIndex(
     function(element){
       console.log("comparando " + element.id + " y " +   req.params.id);
       return element.id == req.params.id
     }
   )

   console.log("indexOfElement es " + indexOfElement);
   if (indexOfElement >= 0) {
     i = indexOfElement;
     flagUserDelete = "true";
   }


  if (flagUserDelete == "true") {
    // Delete user
    users.splice(i, 1);
    io.writeUserDataToFile(users);
    res.send({"msg":"User has been deleted"});
  } else {
    // Not found
    console.log("Don't delete anything not equal");
    res.send({"msg":"User doesn't exist"});
  }
}

module.exports.getUsersV1 = getUsersV1;
module.exports.getUsersV2 = getUsersV2;
module.exports.getUsersByIdV2 = getUsersByIdV2;
module.exports.createUsersV1 = createUsersV1;
module.exports.createUserV2 = createUserV2;
module.exports.deleteUserV1 = deleteUserV1;