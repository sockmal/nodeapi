
// CONSTANTS
const io =require("../utils/io"); // Declaramos el nuevo modulo para recoger las funciones
const requestJson = require('request-json'); // Para cargar la librería de request-json
const crypt =require("../utils/crypt"); // Cargamos librería de encriptación

const baseMLABUrl = "https://api.mlab.com/api/1/databases/apitechumal12ed/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;

// Get Accaunt By ID
function getAccountByIdV2(req, res) {
  console.log("GET /apitechu/v2/account/:id")

  var id = Number.parseInt(req.params.id);
  console.log("The account id to get is " + id);
  var query = "q=" + JSON.stringify({"idUsuario": id});
  console.log("query is " + query);

  var httpClient = requestJson.createClient(baseMLABUrl);
  console.log("Client created");
  console.log(query);
  // Control the response status
  httpClient.get("account?" + query + "&" + mLabAPIKey,
    function(err, resMlab, body) {
      console.log(body);
      if (err) {
        var response = {
          "msg" : "Error getting account"
        }
        res.status(500);
      } else {
        if (body.length > 0) {
          var response = body;
        } else {
          var response = {
            "msg" : "user not found"
          }
          res.status(404);
        }
      }
      res.send(response);
    }
  )
}

module.exports.getAccountByIdV2 = getAccountByIdV2;