# Node API Project

Node API Application to get users with query params options.

![Node API](/images/nodeApiLogo.png)

## Init Node API Project
```bash
npm init
npm install express --save
npm install nodemon --save-dev
```

## For testing install the following packages
npm install mocha --save-dev
npm install chai --save-dev
npm install chai-http --save-dev
npm install request-json --save

## Install library to use env files
npm install dotenv --save

## Start Node API with nodemon
Nodemon restart the service automatically when detect changes in the filesystem

```
npm start
```

## Commands to build and run node API with docker in your localhost

### Build Image
```
docker build -t imageapitechu12edcgt .
```

### Run Image
```
docker run -d -p 3000:3000 imageapitechu12edcgt
```
Test your rest api with postman or other tools.

#### Tag and push image to dockerhub
```
docker tag imageapitechu12edcgt yourdockerhubuser/imageapitechu12edcgt
docker push yourdockerhubuser/imageapitechu12edcgt
```

## Query Params API Options

| $top=3      | Tilter the uers top              |
|-------------|-------------------------------------------------------------|
| $count=true | Count the total users |

Query URL Examples:
```
localhost:3000/apitechu/v1/users?$top=10&$count=true
localhost:3000/apitechu/v1/users?$top=3
localhost:3000/apitechu/v1/users?$count=true
```

## Delete Params API Options

Example with DELETE http request, this example delete the user 29:
```
localhost:3000/apitechu/v1/users/29
```
